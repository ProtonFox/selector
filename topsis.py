#!/usr/bin/python3
#-*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
import csv

class TOPSIS:
    def __init__(self, data = None, weights = None, alts_names = None):
        """Class for a TOPSIS method statistic study."""
        
        self.data = data
        self.weights = weights
        self.alts_names = alts_names
        self.out_mat = None
        self.S = None
        self.criteria = None
    
    
    def set_data(self, new_data):
        """Loads a new data matrix."""
        
        self.data = new_data
    
    
    def set_weights(self, weights, goal, norm=False):
        """Defines the weights for the positive and negative criteria.
        'goal' is a string containing '+' or '-' characters, respectively
        corresponding to a criterion to be maximized or minimized.
        Set 'norm' to True to automatically normalize the weights."""
        
        if len(weights) != len(goal):
            print("Warning - mismatch between weights and goal dimensions")
            return False
        
        l = len(weights)
        pos = np.zeros(l)
        neg = np.zeros(l)
        
        # build weights submatrices
        
        for k in range(l):
            if goal[k] == '+':
                pos[k] = weights[k]
            else:
                neg[k] = weights[k]
        
        # normalize if needed
        
        if norm:
            pos /= np.amax(pos)
            neg /= np.amax(neg)
        
        # build weights matrix and stores it
        
        self.set_weights_matrix(np.array([pos, neg]))
        
        return True
    
    
    def set_weights_matrix(self, new_weights):
        """Loads a new weights matrix. Weights must be normalized."""
        
        if new_weights.shape[0] == 2:
            self.weights = new_weights
        else:
            print("Warning - invalid weight matrix format")
    
    
    def set_alternatives_names(self, names):
        """Loads a new alternatives names matrix.
        `names' is a python array containing string data."""
        
        if len(names) == self.data.shape[0]:
            self.alts_names = names
        else:
            print("Warning - invalid name array format")
    
    
    def data_from_csv(self, path, sep=";", comma=False):
        """Loads data, alternatives names and criteria names from a csv file.
        'sep', defines the data delimiter, set 'comma' to True if the file has
        commas as decimal separator."""
        
        with open(path) as fl:
            reader = csv.reader(fl, delimiter=sep)
            
            # get alternatives names
            
            alternatives = []
            content = []
            for row in reader:
                alternatives.append(row[0])
                content.append(row)
            del alternatives[0]
            
            self.alts_names = alternatives
            
            # get criteria names
            
            criteria = content.pop(0)
            del criteria[0]
            
            self.criteria = criteria
            
            # get data
            
            data = np.zeros((len(content), len(content[0])-1))
            
            for i in range(len(content)):
                if comma:
                    for j in range(1,len(content[i])):
                        content[i][j] = content[i][j].replace(',', '.')
                
                data[i] = np.array(content[i][1:], dtype=float)
            
            self.data = data
        
        fl.close()
            
    
    def evaluate(self):
        """Processes data, finds the scores matrix and the best solution.
        Uses the TOPSIS method. Sources :
        - https://en.wikipedia.org/wiki/TOPSIS
        - YEZZA, A. (2017). La méthode TOPSIS expliquée pas à pas"""
        
        # normalized evaluation matrix
        
        self.out_mat = self.data.astype('float32')
        r_mat = np.zeros(self.out_mat.shape)
        
        L = self.out_mat.shape[0]
        C = self.out_mat.shape[1]
        
        sqr_sum = np.zeros(C)
        for k in self.out_mat:
            sqr_sum += k**2
        sqr_sum = np.sqrt(sqr_sum)
            
        for j in range(L):
            for i in range(C):
                x = self.out_mat[j][i]
                r_mat[j][i] = x / sqr_sum[i]
            
        for i in range(L):
            self.out_mat[i] = r_mat[i] * (self.weights[0] + self.weights[1])
            
        # best ideal solution
        
        Ap = np.zeros(C)
        
        for i in range(C):
            if self.weights[0][i] != 0:
                Ap[i] = max(self.out_mat[:,i])
            else:
                Ap[i] = min(self.out_mat[:,i])
                
        # worst ideal solution
        
        Am = np.zeros(C)
        
        for i in range(C):
            if self.weights[0][i] != 0:
                Am[i] = min(self.out_mat[:,i])
            else:
                Am[i] = max(self.out_mat[:,i])
        
        # distance to ideal solution
        
        Ep = np.zeros(L)
        
        for i in range(L):
            for j in range(C):
                Ep[i] += (Ap[j] - self.out_mat[i][j])**2
        Ep = np.sqrt(Ep)
        
        Em = np.zeros(L)
        
        for i in range(L):
            for j in range(C):
                Em[i] += (Am[j] - self.out_mat[i][j])**2
        Em = np.sqrt(Em)
        
        # similarity to the best condition
        
        S = np.zeros(L)
        
        for i in range(L):
            S[i] = Em[i] / (Ep[i] + Em[i])
        
        self.S = S
    
    
    def get_score_matrix(self):
        """Returns the score / decision matrix.
        TOPSIS.evaluate() must be called before calling this function !
        """
        
        if not self.out_mat.any() == None:
            return self.out_mat
        else:
            print("Warning - TOPSIS.evaluate() must be called before calling this function !")
            
            
    def get_similarity(self):
        """Returns the similarity to the best solution array.
        TOPSIS.evaluate() must be called before calling this function !
        """
        
        if not self.S.any()== None:
            return self.S
        else:
            print("Warning - TOPSIS.evaluate() must be called before calling this function !")
    
    
    def get_weights(self):
        """Returns the weights matrix, if it has been specified."""
        
        if not self.weights.any() == None:
            return self.weights
        else:
            print("Warning - please give a weights matrix using TOPSIS.set_weights() \
                or TOPSIS.set_weights_matrix()")
    
    
    def plot(self, title=None, ylabel="Final score"):
        """Plots the bargraph of the TOPSIS results.
        TOPSIS.evaluate() must be called before calling this function !
        """
        
        if not self.out_mat.any() == None:
            pass
        else:
            print("Warning - TOPSIS.evaluate() must be called before calling this function !")
            return None
        
        y = self.S * 100

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.grid(axis = 'y', linestyle = '--', linewidth = 0.5)
        ax.yaxis.set_major_formatter(mtick.PercentFormatter())
        ax.set_axisbelow(True)
        ax.set_title(title)
        ax.set_ylabel(ylabel)
        barlist = ax.bar(self.alts_names, y)
        barlist[np.argmax(y)].set_color('g')

        for i,j in zip(self.alts_names,y):
            ax.annotate("{} %".format(np.round(j, 1)),xy=(i,j+0.5))

        plt.show()
