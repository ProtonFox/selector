Selector - A Python implementation for the TOPSIS decision-making method
========================================================================

Selector allows you to compare the best alternative suiting your needs, based on their characteristics, your criteria and your weights. This program will then compute the global score of your alternatives and help you to find the best one among them.

This program works using the TOPSIS method, a famous multi-criteria comparison and decision algorithm, introduced by Hwang and Yoon in 1981. More information can be found at : https://en.wikipedia.org/wiki/TOPSIS

To use this program, you will need to give a data matrix, an alternative name array and a weights matrix. See the `TOPSIS` class documentation and test examples in `main.py` for more details.
