#!/usr/bin/python3
#-*- coding: utf-8 -*-

import numpy as np
from topsis import *

### TEST

data = np.array([[12.5, 6400, 30],
                 [12.5, 2000, 8.5],
                 [20, 5500, 18],
                 [49, 6000, 14],
                 [25, 3000, 23]])

w = np.array([[0.0, 0.5, 0.0],
              [0.25, 0.0, 0.6]])

names = ["LMX2572",
         "LMX2572LP",
         "LMX2582",
         "STW81200",
         "HMC832"]

stat = TOPSIS()
#stat.set_alternatives_names(names)
stat.set_weights([0.25, 0.5, 0.6], "-+-")
stat.data_from_csv("./test/test.csv", sep=';', comma=True)
stat.evaluate()
stat.plot("Comparaison des circuits synthétiseurs RF", "Score final")
